var busqueda = 4;
var arbol = new Arbol();

//nivel 1
arbol.nodoPadre.hI=arbol.agregarNodo(arbol.nodoPadre, "Nivel 1 Izquierdo", "16", 16);
arbol.nodoPadre.hD=arbol.agregarNodo(arbol.nodoPadre, "Nivel 1 Derecho", "20", 20);

//nivel 2
arbol.nodoPadre.hI.hI=arbol.agregarNodo(arbol.nodoPadre.hI , "Nivel 2 Izquierdo", "8", 8, nivel=2);
arbol.nodoPadre.hI.hD=arbol.agregarNodo(arbol.nodoPadre.hI , "Nivel 2 Derecho", "8", 8);

arbol.nodoPadre.hD.hI=arbol.agregarNodo(arbol.nodoPadre.hD.hI, "Nivel 2 Izquierdo", "8", 8,nivel=2);
arbol.nodoPadre.hD.hD=arbol.agregarNodo(arbol.nodoPadre.hD.hD, "Nivel 2 Derecho", "12", 12);

//nivel 3
arbol.nodoPadre.hI.hI.hI=arbol.agregarNodo(arbol.nodoPadre.hI.hI.hI , "Nivel 3 Izquierdo", "e", 3);
arbol.nodoPadre.hI.hI.hD=arbol.agregarNodo(arbol.nodoPadre.hI.hI.hD , "Nivel 3 Derecho", "4", 4);

arbol.nodoPadre.hI.hD.hI=arbol.agregarNodo(arbol.nodoPadre.hI.hI.hI , "Nivel 3 Izquierdo", "a", 4);
arbol.nodoPadre.hI.hD.hD=arbol.agregarNodo(arbol.nodoPadre.hI.hI.hD , "Nivel 3 Derecho", "4", 4);

arbol.nodoPadre.hD.hI.hI=arbol.agregarNodo(arbol.nodoPadre.hD.hI.hI, "Nivel 3 Izquierdo", "4", "4");
arbol.nodoPadre.hD.hI.hD=arbol.agregarNodo(arbol.nodoPadre.hD.hI.hD, "Nivel 3 Derecho", "4", "4");

arbol.nodoPadre.hD.hD.hI=arbol.agregarNodo(arbol.nodoPadre.hD.hD.hI, "Nivel 3 Izquierdo", "5", "5");
arbol.nodoPadre.hD.hD.hD=arbol.agregarNodo(arbol.nodoPadre.hD.hD.hD, "Nivel 3 Derecho", "Espacio", "7");

//nivel 4
arbol.nodoPadre.hI.hI.hD.hI=arbol.agregarNodo(arbol.nodoPadre.hI.hI.hD.hI, "Nivel 4 Izquierdo", "n", "2");
arbol.nodoPadre.hI.hI.hD.hD=arbol.agregarNodo(arbol.nodoPadre.hI.hI.hD.hD, "Nivel 4 Derecho", "2", "2");

arbol.nodoPadre.hI.hI.hD.hI=arbol.agregarNodo(arbol.nodoPadre.hI.hI.hD.hI, "Nivel 4 Izquierdo", "n", "2");
arbol.nodoPadre.hI.hI.hD.hD=arbol.agregarNodo(arbol.nodoPadre.hI.hI.hD.hD, "Nivel 4 Derecho", "2", "2");

arbol.nodoPadre.hI.hD.hD.hI=arbol.agregarNodo(arbol.nodoPadre.hI.hD.hD.hI, "Nivel 4 Izquierdo", "t", "2");
arbol.nodoPadre.hI.hD.hD.hD=arbol.agregarNodo(arbol.nodoPadre.hI.hD.hD.hD, "Nivel 4 Derecho", "m", "2");

arbol.nodoPadre.hI.hD.hD.hD=arbol.agregarNodo(arbol.nodoPadre.hI.hD.hD.hD, "Nivel 4 Derecho", "m", "2");

arbol.nodoPadre.hD.hI.hI.hI=arbol.agregarNodo(arbol.nodoPadre.hD.hI.hI.hI, "Nivel 4 Izaquierdo", "i", "2");
arbol.nodoPadre.hD.hI.hI.hD=arbol.agregarNodo(arbol.nodoPadre.hD.hI.hI.hD, "Nivel 4 Derecho", "2", "2");

arbol.nodoPadre.hD.hI.hD.hI=arbol.agregarNodo(arbol.nodoPadre.hD.hI.hD.hI, "Nivel 4 Izquierdo", "h", "2");
arbol.nodoPadre.hD.hI.hD.hD=arbol.agregarNodo(arbol.nodoPadre.hD.hI.hD.hD, "Nivel 4 Derecho", "s", "2");

arbol.nodoPadre.hD.hD.hI.hI=arbol.agregarNodo(arbol.nodoPadre.hD.hD.hI.hI, "Nivel 4 Izquierdo", "2","2");
arbol.nodoPadre.hD.hD.hI.hD=arbol.agregarNodo(arbol.nodoPadre.hD.hD.hI.hD, "Nivel 4 Derecho", "f","3");

//Nivel 5
arbol.nodoPadre.hI.hI.hD.hD.hI=arbol.agregarNodo(arbol.nodoPadre.hI.hI.hD.hD.hI, "Nivel 5 Izquierdo", "o","1");
arbol.nodoPadre.hI.hI.hD.hD.hD=arbol.agregarNodo(arbol.nodoPadre.hI.hI.hD.hD.hD, "Nivel 5 Derecho", "u","1");

arbol.nodoPadre.hD.hI.hI.hD.hI=arbol.agregarNodo(arbol.nodoPadre.hD.hI.hI.hD.hI, "Nivel 5 Izquierdo", "x","1");
arbol.nodoPadre.hD.hI.hI.hD.hD=arbol.agregarNodo(arbol.nodoPadre.hD.hI.hI.hD.hD, "Nivel 5 Derecho", "p","1");

arbol.nodoPadre.hD.hD.hI.hI.hI=arbol.agregarNodo(arbol.nodoPadre.hD.hD.hI.hI.hI, "Nivel 5 Izquierdo", "r","1");
arbol.nodoPadre.hD.hD.hI.hI.hD=arbol.agregarNodo(arbol.nodoPadre.hD.hD.hI.hI.hD, "Nivel 5 Derecho", "l","1");


var resultado = arbol.verificarNivelHijos(arbol.nodoPadre);
var busqueda = arbol.buscarValor(busqueda,arbol.nodoPadre);
console.log(busqueda);
//console.log("nivel: " + nivel + ": " + buscarNodos);
var caminoNodo = arbol.buscarCaminoNodo(busqueda);
//console.log(caminoNodo);
var suma = arbol.sumarCaminoNodo(busqueda);
console.log(suma);

